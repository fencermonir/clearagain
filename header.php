<?php
$themeTemplateDirectoryUrl = get_template_directory_uri();
global $cla_theme;
$logo = (!empty($cla_theme['cla-media-logo']['url'])) ? $cla_theme['cla-media-logo']['url'] : $themeTemplateDirectoryUrl.'/assets/img/header_logo.png';
$topbar_enable = $cla_theme['top-bar-enable'];
$email = $cla_theme['top-email'];
$address = $cla_theme['top-address'];
$phone = $cla_theme['top-phone'];
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"><![endif]-->
<html <?php language_attributes(); ?>>
  <head>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width">
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php
                if ( is_front_page()) {
                    bloginfo('name');
                } elseif (is_category()) {
                    echo 'Category:'; wp_title(); echo ' | '; bloginfo('name');
                } elseif (is_search()) {
                    echo 'Search Results'; echo ' | '; bloginfo('name');
                } elseif ( is_day() || is_month() || is_year() ) {
                    echo 'Archives:'; wp_title(); echo ' | '; bloginfo('name');
                } else {
                    echo wp_title(); echo ' | '; bloginfo('name');
                }
        ?></title>
        <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="cla_main-header area">
      <?php if( $topbar_enable ) { ?>
      <section class="cla_header-top-bar-section area" style="background: #dddddd;">
        <article class="container">
          <div class="row">
            <div class="col-sm-9">
              <div class="cla_top-bar-left">
                <?php if( !empty($email) ) : ?>
                <span><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></span>
              <?php endif; ?>
              <?php if( !empty($address) ) : ?>
                <span><i class="fa fa-map-marker"></i><?php echo $address; ?></span>
              <?php endif; ?>
              </div>
              <!-- End of cla_top-bar-left -->
            </div>
            <!-- End of col -->
            <div class="col-sm-3">
              <div class="cla_top-bar-right">
                <?php if( !empty($phone) ) : ?>
                <span><i class="fa fa-phone-square" aria-hidden="true"></i><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></span>
              <?php endif; ?>
              </div>
              <!-- End of cla_top-bar-right -->
            </div>
            <!-- End of col -->
          </div>
          <!-- End of row -->
        </article>
        <!-- End of container -->
      </section>
      <!-- End of cla_header-top-bar-section -->
    <?php } ?>
      <section class="cla_header-section area">
        <article class="container">
          <div class="row">
            <div class="col-sm-3">
              <div class="header-logo area">
                <a href="<?php echo home_url('/'); ?>" class="navbar-brand"><img src="<?php echo $logo; ?>" alt="logo-img" class="img-responsive"></a>
              </div>
              <!-- End of header-logo -->
            </div>
            <!-- End of col -->
            <div class="col-sm-9">
              <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <!--End of navbar-header-->
              <?php
               $menu_arguments = array(
                 'theme_location' => 'primary_navigation',
                 'container' => 'div',
                 'container_class' => 'collapse navbar-collapse',
                 'menu_class' => 'nav navbar-nav navbar-right header-menu',
                 'echo' => true,
                 'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                 'depth' => 3,
                 'walker' => new wp_bootstrap_navwalker()
               );
             wp_nav_menu( $menu_arguments );
             ?>
            </div>
            <!-- End of col -->
          </div>
          <!-- End of row -->
        </article>
        <!--End of container-->
      </section>
      <!--End of header-->
    </header>
    <!--End of header-->
    <main class="area">
