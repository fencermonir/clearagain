<?php
get_header();
$hideTitle=get_post_meta( get_the_ID(), 'hide_title', true );
if($hideTitle == 'yes') {
?>
<section class="title-header">
  <h2 class="media-heading text-center"><?php the_title(); ?></h2>
</section>
<?php } ?>
<section class="default-page-without-sidebar">
  <div class="container page-body">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php
      if(have_posts()):
        while ( have_posts() ) :
          the_post();
          the_content();
        endwhile;
      endif;
      ?>
    </article>
  </div>
</section>
<?php get_footer();
