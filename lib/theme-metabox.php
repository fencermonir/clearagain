<?php
/* ---------------------------------------------------------------------------
* Meta Box: Hide Page Title
* -------------------------------------------------------------------------- */
add_action( 'add_meta_boxes', 'add_page_title_checkbox_function' );
function add_page_title_checkbox_function() {
	add_meta_box('hide_page_title_id','SHOW PAGE TITLE ?', 'page_title_checkbox_callback_function', 'page', 'side', 'high');
}
function page_title_checkbox_callback_function( $post ) {
	global $post;
	$hideTitle=get_post_meta( $post->ID, 'hide_title', true );?>
	<?php wp_nonce_field('clearagain_sidebar_save_meta_data', 'clearagain_sidebar_class_nonce' ); ?>
	<input type="checkbox" name="hide_title" value="yes" <?php echo (($hideTitle=='yes') ? 'checked="checked"': '');?>/> YES
	<?php
}
add_action('save_post', 'save_hide_title');
function save_hide_title($post_id){
  if ( ! isset( $_POST['clearagain_sidebar_class_nonce'] ) ) {
    return;
}
if ( ! wp_verify_nonce( $_POST['clearagain_sidebar_class_nonce'], 'clearagain_sidebar_save_meta_data' ) ) {
    return;
}
if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }
	$hide_title = isset($_POST['hide_title']) ? $_POST['hide_title'] : 'no';
	update_post_meta($post_id, 'hide_title', $hide_title);
}
// page side bar
add_action( 'add_meta_boxes', 'add_page_meta_box_function' );
function add_page_meta_box_function() {
  add_meta_box('sidebar-list', 'Select Sidebar', 'registered_sidebar_function', 'page', 'side', 'high');
}
// page sidebar callback function
function registered_sidebar_function(){
    global $post;
    $sidebarName = get_post_meta($post->ID, 'sidebar_name', true);
    wp_nonce_field('clearagain_sidebar_save_meta_data', 'clearagain_sidebar_class_nonce' );?>
    <select name="sidebar_name">
    <?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
         <option value="<?php echo $sidebar['id']; ?>" <?php  if($sidebar['id']== $sidebarName){ echo "selected='selected'";} ?> >
                  <?php echo ucwords( $sidebar['name'] ); ?>
         </option>
    <?php } ?>
    </select>
    <?php
}

// page sidebar save function
add_action('save_post', 'save_page_meta_info');
    function save_page_meta_info(){
       global $post;
       if ( ! isset( $_POST['clearagain_sidebar_class_nonce'] ) ) {
         return;
     }
     if ( ! wp_verify_nonce( $_POST['clearagain_sidebar_class_nonce'], 'clearagain_sidebar_save_meta_data' ) ) {
         return;
     }
     if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
         return;
       }
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
      update_post_meta($post->ID, "sidebar_name", $_POST["sidebar_name"]);
    }
}
