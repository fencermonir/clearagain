<?php
/*
* display year shortcode
*
*/
add_shortcode('display_year','display_year_shortcode_callback');
function display_year_shortcode_callback()
{
  return date('Y');
}
/*
* display social share shortcode
*
*/
add_shortcode('display_social_share','display_social_share_shortcode_callback');
function display_social_share_shortcode_callback()
{
  ob_start();
  global $cla_theme;
  $style = '';
  $font_color = (!empty($cla_theme['cla-social-font-color'])) ? $cla_theme['cla-social-font-color'] : '';
  $background_color = (!empty($cla_theme['cla-social-background-color'])) ? $cla_theme['cla-social-background-color'] : '';
  if(!empty($font_color)) {
    $style .= "color: $font_color; ";
  }
  if(!empty($background_color)) {
    $style .= "background-color: $background_color; ";
  }
  $facebook = (!empty($cla_theme['cla-facebook'])) ? $cla_theme['cla-facebook'] : '';
  $twitter = (!empty($cla_theme['cla-twitter'])) ? $cla_theme['cla-twitter'] : '';
  $instagram = (!empty($cla_theme['cla-instagram'])) ? $cla_theme['cla-instagram'] : '';
  $linkedin = (!empty($cla_theme['cla-linkedin'])) ? $cla_theme['cla-linkedin'] : '';
  $gplus = (!empty($cla_theme['cla-gplus'])) ? $cla_theme['cla-gplus'] : '';
  $pinterest = (!empty($cla_theme['cla-pinterest'])) ? $cla_theme['cla-pinterest'] : '';
  ?>
  <div class="pix__social_widgets">
    <ul>
    <?php if(!empty($facebook)) : ?>
      <li><a  href="<?php echo $facebook; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-facebook"></i></a></li>
    <?php endif; ?>
    <?php if(!empty($twitter)) : ?>
      <li><a  href="<?php echo $twitter; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-twitter"></i></a></li>
    <?php endif; ?>
    <?php if(!empty($instagram)) : ?>
      <li><a  href="<?php echo $instagram; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-instagram"></i></a></li>
    <?php endif; ?>
    <?php if(!empty($linkedin)) : ?>
      <li><a  href="<?php echo $linkedin; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-linkedin"></i></a></li>
    <?php endif; ?>
    <?php if(!empty($gplus)) : ?>
      <li><a  href="<?php echo $gplus; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-google-plus"></i></a></li>
    <?php endif; ?>
    <?php if(!empty($pinterest)) : ?>
      <li><a  href="<?php echo $pinterest; ?>"<?php if( !empty($style)) { echo 'style="'.$style.'"'; } ?>><i class="fa fa-pinterest-p"></i></a></li>
    <?php endif; ?>
    </ul>
  </div>
  <?php

  return ob_get_clean();
}
