<?php get_header(); ?>
<section class="title-header">
  <h2 class="media-heading text-center"><?php the_title(); ?></h2>
</section>
<section class="cla_blog-section cla_section-padding area">
  <article class="container">
    <div class="row">
      <?php if(get_post_type() == 'post'){ ?>
      <div class="col-sm-8">
        <?php
          if(have_posts()){
            while (have_posts()) {
              the_post();
              $image_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
              $image_alt = (empty($image_alt)) ? get_the_title($post->ID) : $image_alt;
              $attachment_title = get_the_title(get_post_thumbnail_id($post->ID));
              ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="cla_blog-posts area">
          <div class="cla_single-blog-post cla_post-details area">
            <!-- <h2 class="cla_post-title"><?php //the_title(); ?></h2> -->
            <div class="cla_single-post-content area">
              <?php
              if(has_post_thumbnail()){
                $thumbImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
              <div class="cla_single-post-img">
                <img src="<?php echo $thumbImage; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $attachment_title; ?>">
              </div>
              <!-- End of post img -->
            <?php }else{
              ?>
              <div class="cla_single-post-img cla_cpt-img">
                <img src="<?php echo get_template_directory_uri().'/assets/img/placeholder.jpg'; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $attachment_title; ?>">
              </div> 
              <?php
            } ?>
              <div class="cla_single-post-elements area">
                <span><a href="#"><i class="fa fa-calendar"></i> <?php the_time('F jS, Y',$post->ID); ?></a></span>
                <span><i class="fa fa-comments"></i> <?php echo get_comments_number($post->ID); ?> Comments</span>
              </div>
              <!-- End of post elements -->
              <div class="cla_single-post-text area">
                <?php the_content(); ?>
              </div>
              <!-- End of post text -->
            </div>
            <!-- End of post content box -->
            <?php
              $categories_list = get_the_category_list(  ', '  );
              if( $categories_list ) {
            ?>
              <div class="cla_single-post-cat area">
                <i class="fa fa-tag"></i>
                <?php printf( esc_html__( '%1$s', 'clearagain' ), $categories_list ); ?>
                <?php
                $tag_list = get_the_tag_list( '', __( ', ', 'clearagain' ) );
                if ( '' != $tag_list ) {
                    echo '<br/>TAGGED : '. $tag_list ;
                }
                ?>
              </div>
            <!-- End of post cat -->
              <?php } ?>
          </div>
          <!-- End of single blog post -->
        </div>
        <!-- End of Blog posts -->
      </article>
      <?php
      }
      wp_reset_query();
    }
  ?>
  <div class="cla-comments">
    <?php if ( comments_open() || get_comments_number() ) {
             comments_template();
          }
    ?>
  </div>
      </div>
      <!-- End of row -->
      <div class="col-sm-4 cla_blog-sidebar">
        <?php
        if( is_active_sidebar( 'blog-sidebar' ) ) {
          dynamic_sidebar('blog-sidebar');
        }
        ?>
      </div>
      <!-- End of col -->
      <?php } else {
        ?>
      <div class="col-sm-12">
      <?php
          if(have_posts()){
            while (have_posts()) {
              the_post();
              $image_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
              $image_alt = (empty($image_alt)) ? get_the_title($post->ID) : $image_alt;
              $attachment_title = get_the_title(get_post_thumbnail_id($post->ID));
              ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="cla_blog-posts area">
          <div class="cla_single-blog-post cla_post-details area">
            <!-- <h2 class="cla_post-title"><?php //the_title(); ?></h2> -->
            <div class="cla_single-post-content area">
              
              <?php
              if(has_post_thumbnail()){
                $thumbImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
              <div class="cla_single-post-img cla_cpt-img">
                <img src="<?php echo $thumbImage; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $attachment_title; ?>">
              </div>
              <!-- End of post img -->
            <?php }else {
              ?>
              <div class="cla_single-post-img cla_cpt-img">
                <img src="<?php echo get_template_directory_uri().'/assets/img/placeholder.jpg'; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $attachment_title; ?>">
              </div>
              <?php

            } ?>
              <!-- <div class="cla_single-post-elements area">
                <span><a href="#"><i class="fa fa-calendar"></i> <?php the_time('F jS, Y',$post->ID); ?></a></span>
                <span><i class="fa fa-comments"></i> <?php echo get_comments_number($post->ID); ?> Comments</span>
              </div> -->
              <!-- End of post elements -->
              <div class="cla_single-post-text area">
                <?php the_content(); ?>
              </div>
              <!-- End of post text -->
            </div>
            <!-- End of post content box -->
            <?php
              $categories_list = get_the_category_list(  ', '  );
              if( $categories_list ) {
            ?>
              <div class="cla_single-post-cat area">
                <i class="fa fa-tag"></i>
                <?php printf( esc_html__( '%1$s', 'clearagain' ), $categories_list ); ?>
                <?php
                $tag_list = get_the_tag_list( '', __( ', ', 'clearagain' ) );
                if ( '' != $tag_list ) {
                    echo '<br/>TAGGED : '. $tag_list ;
                }
                ?>
              </div>
            <!-- End of post cat -->
              <?php } ?>
          </div>
          <!-- End of single blog post -->
        </div>
        <!-- End of Blog posts -->
      </article>
      <?php
      }
      wp_reset_query();
    }
  ?>
  <div class="cla-comments">
    <?php if ( comments_open() || get_comments_number() ) {
             comments_template();
          }
    ?>
  </div>
      </div>
      <!-- End of col -->
      <?php }?>
    </div>
    <!-- End of row -->
  </article>
  <!-- End of container -->
</section>
<!-- End of blog section -->
<?php get_footer(); 