<?php
$themeTemplateDirectoryUrl = get_template_directory_uri();
global $cla_theme;
$widget_color = $cla_theme['cla-footerbottom-color'];
$copyright = $cla_theme['cla-copyright'];
$footer_script = $cla_theme['cla-footer-script'];
?>
</main>
<!--End of main-->
<footer class="area cla_main-footer" style="background: <?php echo $widget_color; ?>">
  <section class="cla_footer-top-section cla_section-padding area">
    <article class="container">
      <div class="row cla_row">
        <?php if( is_active_sidebar( 'footer-widget-area-1' ) ) : ?>
        <div class="col-sm-4">
          <?php dynamic_sidebar('footer-widget-area-1'); ?>
        </div>
      <?php endif; ?>
        <?php if( is_active_sidebar( 'footer-widget-area-2' ) ) : ?>
        <div class="col-sm-3 col-sm-offset-1">
          <?php dynamic_sidebar('footer-widget-area-2'); ?>
        </div>
      <?php endif; ?>
        <?php if( is_active_sidebar( 'footer-widget-area-3' ) ) : ?>
        <div class="col-sm-4">
          <?php dynamic_sidebar('footer-widget-area-3'); ?>
        </div>
      <?php endif; ?>
      </div>
      <!-- End of row -->
    </article>
    <!--End of container-->
  </section>
  <!--End of footer top section-->
  <section class="footer-bottom-section area">
    <article class="container text-center">
      <p class="copyright-text" style="font-size: 14px; color: #ffffff;margin: 0;"><?php echo do_shortcode($copyright); ?></p>
    </article>
    <!-- End of container -->
  </section>
  <!-- End of footer bootom section -->
</footer>
<!--End of footer-->
<?php wp_footer();
if(!empty($footer_script)){
echo $footer_script;
}
?>
</body>
</html>
