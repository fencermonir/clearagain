<?php get_header();
$postspage_id = get_option('page_for_posts');
$hideTitle = get_post_meta( $postspage_id, 'hide_title', true );
?>
<section class="title-header">
  <h2 class="media-heading text-center"><?php echo get_the_title($postspage_id); ?></h2>
</section>
<section class="cla_blog-section cla_section-padding area">
  <article class="container">
    <div class="row">
      <div class="col-sm-8">
        <?php
        if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
        elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
        else { $paged = 1; }

        $args = array(
         'post_type' => 'post',
         'post_status' => 'publish',
         'posts_per_page' => get_option('posts_per_page'),
         'paged' => $paged
         );
        $wp_query = new WP_Query($args);
        if(have_posts()):
        while ( have_posts() ) : the_post();
        $image_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
        $image_alt = (empty($image_alt)) ? get_the_title($post->ID) : $image_alt;
        $attachment_title = get_the_title(get_post_thumbnail_id($post->ID));
       ?>
       <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="cla_blog-posts area">
          <div class="cla_single-blog-post area">
            <a href="<?php the_permalink(); ?>"><h2 class="cla_post-title"><?php the_title(); ?></h2></a>
            <div class="cla_single-post-content area">
              <?php
              if ( has_post_thumbnail() ) {
                $thumbImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
              }else {
                $thumbImage = get_template_directory_uri().'/assets/img/placeholder.jpg';
              }
              ?>
              <div class="cla_single-post-img">
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $thumbImage; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $attachment_title; ?>"></a>
              </div>
            <?php //} ?>
              <!-- End of post img -->
              <div class="cla_single-post-text">
                <?php
                $custom_more = get_post_meta($post->ID, "custom_more_text", true);
                $custom_excerpt = get_the_excerpt();
                if( $custom_more && $custom_more != '' ){
                  echo '<p>'. the_content( '', TRUE ).'</p>';
                } else {
                  echo '<p>'.$custom_excerpt.'</p>';
                }
                ?>
                <a href="<?php the_permalink(); ?>" class="cla_cta-btn cla_theme-button">read more</a>
              </div>
              <!-- End of post text -->
            </div>
            <!-- End of post content box -->
            <div class="cla_single-post-elements area">
              <span><a href="#"><i class="fa fa-calendar"></i> <?php echo get_the_date('F j, Y',$post->ID); ?></a></span>
              <span><a href=""><i class="fa fa-comments"></i> <?php echo get_comments_number($post->ID); ?> Comments</a></span>
              <?php
                $categories_list = get_the_category_list(  ', '  );
                if( $categories_list ) {
              ?>
              <div class="cla_single-post-cat">
                <i class="fa fa-tag"></i>
                <?php printf( esc_html__( '%1$s', 'clearagain' ), $categories_list ); ?>
              </div>
            <?php } ?>
            </div>
            <!-- End of post elements -->
          </div>
          <!-- End of single blog post -->
        </div>
        <!-- End of Blog posts -->
      </article>
    <?php endwhile;
    wp_reset_postdata();
    endif;
    global $wp_query;
    $total_pages = $wp_query->max_num_pages;
    if ($total_pages > 1){
      $current_page = max(1, get_query_var('paged'));
      echo '<div class="blog-pagination text-center"><ul class="pagination"> <li>';
      echo paginate_links(
        array(
          'base' => get_pagenum_link(1) . '%_%',
          'format' => 'page/%#%/',
          'current' => $current_page,
          'total' => $total_pages,
          'prev_text' => '« Prev',
          'next_text' => 'Next »'
          )
        );
      echo '</li> </ul></div>';
    }
    ?>
      </div>
      <!-- End of row -->
      <div class="col-sm-4 cla_blog-sidebar">
        <?php
        if( is_active_sidebar( 'blog-sidebar' ) ) {
          dynamic_sidebar('blog-sidebar');
        }
        ?>
      </div>
      <!-- End of row -->
    </div>
    <!-- End of row -->
  </article>
  <!-- End of container -->
</section>
<!-- End of blog section -->
<?php get_footer();
