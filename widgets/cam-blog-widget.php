<?php
class CAM_recent_posts_Widget extends WP_Widget {

	//setup the widget name, description, etc...
		public function __construct() {

			$widget_ops = array(
				'classname' => 'cam-recent-blog',
				'description' => 'A list of latest blogs. ',
				);
			parent::__construct( 'cam-recent-blog-widget', 'CAM Recent Blogs', $widget_ops );
		}



	//back-end display of widget
		public function form( $instance ) {
      $title = isset($instance['title']) ? $instance['title']: '';
      $posts_number = isset($instance['posts_number']) ? $instance['posts_number'] : 5;
			?>
      <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label></br>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>">
      </p>

			<p>
				<label for="<?php echo $this->get_field_id('posts_number'); ?>">Number of posts to show:</label>
				<input class="tiny-text" id="<?php echo $this->get_field_id('posts_number'); ?>" name="<?php echo $this->get_field_name('posts_number'); ?>" type="number" step="1" min="1" value="<?php echo $posts_number; ?>" size="3">
			</p>
			<?php
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
      $instance['title'] = $new_instance['title'];
      $instance['posts_number'] = $new_instance['posts_number'];

			return $instance;
		}

	//front-end display of widget
		public function widget( $args, $instance ){
			$title = $instance['title'];
			$numberOfBlogs = $instance['posts_number'];
      echo $args['before_widget'];
      ?>
			  <h2 class="footer-title"><?php echo $title; ?></h2>
					<?php
					$listings = new WP_Query();
					$listings->query('post_type=post&posts_per_page=' . $numberOfBlogs );
					if($listings->found_posts > 0) {
						while ($listings->have_posts()) {
							$listings->the_post();
							$listItem = '<div class="media">
								<div class="media-left">
									<a href="'.get_permalink().'">
									'.get_the_post_thumbnail( get_the_ID(), array(65,65), array( "class" => "media-object" ) ).'
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4>
									<p>'.get_the_date().'</p>
								</div>
							</div>';
							echo $listItem;
						}
					wp_reset_postdata();
				}else{
					echo '<p style="padding:25px;">No blogs found</p>';
				}
    		echo $args['after_widget'];
    }
  }

/*
============================
widget action hook
============================
*/
add_action( 'widgets_init', function() {
	register_widget( 'CAM_recent_posts_Widget' );
} );
