<?php
/**
* Template Name: With Sidebar
*
**/
get_header();
$page_id = get_the_ID();
$hideTitle=get_post_meta( $page_id, 'hide_title', true );
if($hideTitle == 'yes') {
?>
<section class="title-header">
  <h2 class="media-heading p-v-sm text-center"><?php the_title(); ?></h2>
</section>
<?php } ?>
<section class="default-page-with-sidebar">
  <div class="container page-body">
    <div class="row">
      <div class="col-sm-8">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php
          if(have_posts()):
            while ( have_posts() ) :
              the_post();
              the_content();
            endwhile;
          endif;
          ?>
        </article>
      </div>
      <div class="col-sm-4">
        <?php
        $sidebarName = get_post_meta($page_id, 'sidebar_name', true);
        if( is_active_sidebar( $sidebarName ) ){
          dynamic_sidebar( $sidebarName );
        }
        ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
