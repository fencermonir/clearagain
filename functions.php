<?php
 $themeTemplateDirectoryUrl = get_template_directory_uri();
 require_once('lib/wp_bootstrap_navwalker.php');
 require_once('lib/theme-cpt.php');
 require_once('lib/theme-metabox.php');
 require_once('lib/theme-shortcode.php');
 require_once('lib/vc-map.php');
 require_once('widgets/cam-blog-widget.php');
 require_once('lib/class-tgm-plugin-activation.php');
 require_once('lib/example.php');
if( !class_exists('ReduxFrameworkPlugin') ){
	 require_once('lib/redux-framework/redux-framework.php');
	 require_once('lib/redux-framework/sample/config.php');
}
/* -----------------------------------------------------------------------------
* After Theme Setup
* -------------------------------------------------------------------------- */
add_action('after_setup_theme', 'support_theme_features');
function support_theme_features(){
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5');
	add_filter( 'excerpt_length', 'excerpt_length_callback_function' );
	add_filter( 'excerpt_more', 'read_more_callback_function' );
	register_nav_menus( array(
		'primary_navigation' => __('Primary Navigation','clearagain-media'),
		// 'footer_navigation' => __('Footer Navigation','clearagain-media'),
		) );

//load_theme_textdomain( 'clearagain' , get_template_directory().'/languages' );
}

/* -----------------------------------------------------------------------------
* Excerpt Length
* -------------------------------------------------------------------------- */

function excerpt_length_callback_function(){
	return 32;
}
/* -----------------------------------------------------------------------------
* Excerpt More
* -------------------------------------------------------------------------- */
function read_more_callback_function(){
	return '...';
}


add_action('widgets_init', 'widgets_callback_function');
function widgets_callback_function(){
	register_sidebar(
		array(
			'name'          => __( 'General Sidebar', 'clearagain' ),
			'id'            => 'general-sidebar',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
			)
		);
	register_sidebar(
		array(
			'name'          => __( 'Blog Sidebar', 'clearagain' ),
			'id'            => 'blog-sidebar',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
			)
		);
	register_sidebar(
		array(
			'name'          => __( 'Footer Widget Area Social', 'clearagain' ),
			'id'            => 'footer-widget-area-social',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="%2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="footer-title">',
      'after_title' => '</h3>',
			)
		);
	register_sidebar(
		array(
			'name'          => __( 'Footer Widget Area 1', 'clearagain' ),
			'id'            => 'footer-widget-area-1',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="cla_footer-content %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="cla_footer-title">',
      'after_title' => '</h3>',
			)
		);
	register_sidebar(
		array(
			'name'          => __( 'Footer Widget Area 2', 'clearagain' ),
			'id'            => 'footer-widget-area-2',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="cla_footer-content %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="cla_footer-title">',
      'after_title' => '</h3>',
			)
		);
	register_sidebar(
		array(
			'name'          => __( 'Footer Widget Area 3', 'clearagain' ),
			'id'            => 'footer-widget-area-3',
			'description'   => '',
			'class'         => 'sidebar',
      'before_widget' => '<aside id="%1$s" class="cla_footer-content %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="cla_footer-title">',
      'after_title' => '</h3>',
			)
		);
  }

/* -----------------------------------------------------------------------------
* Enqueue Scripts
* -------------------------------------------------------------------------- */

add_action( 'wp_enqueue_scripts', 'clearagain_enqueue_scripts_callback_function');

function clearagain_enqueue_scripts_callback_function(){
	$themeTemplateDirectoryUrl = get_template_directory_uri();
  wp_enqueue_style('clearagain-bootstrap-css', $themeTemplateDirectoryUrl.'/assets/css/bootstrap.min.css');
	wp_enqueue_style('clearagain-font-style-css', $themeTemplateDirectoryUrl.'/assets/css/font-awesome.min.css');
  wp_enqueue_style('clearagain-app-css', $themeTemplateDirectoryUrl.'/assets/css/app.css');
  wp_enqueue_style('clearagain-main-css', $themeTemplateDirectoryUrl.'/assets/css/main.css');
  wp_enqueue_style('clearagain-custom-css', $themeTemplateDirectoryUrl.'/style.css');
  wp_enqueue_style('clearagain-responsive-css', $themeTemplateDirectoryUrl.'/assets/css/responsive.css');

  if ( ! is_admin() && !wp_script_is( 'jquery' ) ) {
      wp_enqueue_script('jquery');
	}
	wp_enqueue_script('clearagain-bootstrap-js', $themeTemplateDirectoryUrl.'/assets/js/bootstrap.min.js', array('jquery'), '', true);
  wp_enqueue_script('clearagain-main-js', $themeTemplateDirectoryUrl.'/assets/js/main.js', array('jquery'), '', true);

//  wp_localize_script( 'clearagain-main-js', 'query_posttype_script', array('ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_head', 'cla_theme_style');
function cla_theme_style() {
  global $cla_theme;
  $theme_color = $cla_theme['cla-button-color'];
  $theme_hover_color = $cla_theme['cla-button-hover-color'];

  $style = '<style>';
  $style .= '.cla_theme-button { background: '.$theme_color.' !important; }';
  $style .= '.cla_theme-button:hover { background: '.$theme_hover_color.' !important; }';
  $style .= '</style>';

  echo trim($style);
}
// VC Link String Array
if(!function_exists('vc_extract_string')){
	function vc_extract_string( $input ){
	$font_elements = explode('|',$input);
	$final_array =  array();
	foreach ($font_elements as $font_element) {
		if( ! empty($font_element)){
			$new_array = explode(':',$font_element);
			$final_array[$new_array[0]] = urldecode($new_array[1]);
		}
	}
	return $final_array;
}
}