<form action="<?php echo home_url('/'); ?>" class="cla_sidebar-searchbar" method="get">
  <input type="text" class="cla_input-filed" placeholder="<?php _e('Search...','clearagain'); ?>"  name="s" value="<?php echo get_search_query(); ?>">
  <button type="submit" class="cla_search-btn"><i class="fa fa-search"></i></button>
</form>
